#
# Regular cron jobs for the qt6-virtualkeyboard package
#
0 4	* * *	root	[ -x /usr/bin/qt6-virtualkeyboard_maintenance ] && /usr/bin/qt6-virtualkeyboard_maintenance
