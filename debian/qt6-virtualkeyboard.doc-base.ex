Document: qt6-virtualkeyboard
Title: Debian qt6-virtualkeyboard Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-virtualkeyboard is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-virtualkeyboard/qt6-virtualkeyboard.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-virtualkeyboard/qt6-virtualkeyboard.ps.gz

Format: text
Files: /usr/share/doc/qt6-virtualkeyboard/qt6-virtualkeyboard.text.gz

Format: HTML
Index: /usr/share/doc/qt6-virtualkeyboard/html/index.html
Files: /usr/share/doc/qt6-virtualkeyboard/html/*.html
